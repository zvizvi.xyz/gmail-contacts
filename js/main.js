let isShortcutAdded;

function setupShortcut () {
  if (isShortcutAdded) {
    $(document).off('mouseover mouseout', setupShortcut);
    return;
  }

  let hostList = {
    'mail.google.com': {
      div: 'brC-bsf-aT5-aOt',
      loadingClassName: 'aT5-aOt-I-JE'
    },
    'calendar.google.com': {
      div: 'Kk7lMc-Ku9FSb-DWWcKd-OomVLb',
      loadingClassName: 'DWWcKd-OomVLb-LgbsSe-OWB6Me'
    },
    'drive.google.com': {
      div: 'Kk7lMc-Ku9FSb-Yb-Il'
    },
    'docs.google.com': {
      div: 'companion-guest-app-switcher',
      loadingClassName: 'app-switcher-button-disabled'
    }
  };

  let div = $('.' + hostList[location.host].div);
  if (!div.length) {
    return;
  }
  if ((hostList[location.host].loadingClassName && div.find('.' + hostList[location.host].loadingClassName).length)) {
    return;
  }

  let className = [div.find('div:first').attr('class'), div.find('div:first > div:first-child').attr('class'), div.find('div:first > div:nth-child(2)').attr('class'), div.find('div:first > div:nth-child(3)').attr('class'), div.find('div:first > div:last').attr('class')];
  let content = `<a class="contacts_shortcut" href="https://contacts.google.com/" target="_blank">
    <div class="${className[0]}" role="tab" id="gsc-gab-contacts" aria-label="Contacts" aria-selected="false" style="user-select: none;">
      <div class="${className[1]}" style="user-select: none;"></div>
      <div class="${className[3]}" style="background-image: url(&quot;https://ssl.gstatic.com/images/branding/product/1x/contacts_64dp.png&quot;); user-select: none;"></div>
    </div>
  </a>`;
  div.prepend(content);

  let hoverClassNameList = {
    'mail.google.com': 'aT5-aOt-I-JW',
    'calendar.google.com': 'DWWcKd-OomVLb-LgbsSe-ZmdkE',
    'drive.google.com': 'Yb-Il-d-W'
  };
  let hoverClassName = hoverClassNameList[location.host] || '';
  setTimeout(() => {
    $('.contacts_shortcut > div')
      .mouseover(function () {
        setTimeout(() => {
          $(this).addClass(hoverClassName);
          $('.brC-ays').show().text('Contacts').css({ 'visibility': 'visible', 'left': $(this).offset().left, 'top': $(this).offset().top + $(this).height() });
        }, 200);
      })
      .mouseleave(function () {
        setTimeout(() => {
          $(this).removeClass(hoverClassName);
          $('.brC-ays').hide();
        }, 200);
      });
  });
  isShortcutAdded = true;
}

$(document).on('ready mouseover mouseout', setupShortcut);
$(document).ready(() => { setTimeout(setupShortcut, 2000); });
